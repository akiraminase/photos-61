/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Optional;

import app.Photo;
import app.Tag;
import app.User;

/**
 * controller to edit photo
 * @author Ang Gao
 * @author Isaac Chun
 */

public class editPhotoController {

    @FXML Button save;
    @FXML Button cancel;
    @FXML Text pathText;
    @FXML TextArea newCaption;
    @FXML TextArea newTags;
    @FXML ListView tempTagList;
    @FXML ChoiceBox tagNameDrop;
    @FXML TextField tagValueField;
    @FXML Button addTag;
    @FXML Button deleteTag;
    
    String captionToAdd = "";
    ArrayList<Tag> tempTags = new ArrayList<Tag>();//tag list to add
    User user;
    int storageIndex;
    Photo photo;

    private ObservableList<String> obsList;

    
    /** 
     * @param mainStage
     * @throws Exception
     */
    public void start(Stage mainStage) throws Exception{
        user = User.readUserSession();
        String path = user.getAlbumByIndex(user.selectedAlbumIndex).getPhotoPathByIndex(user.selectedPhotoIndex);
        pathText.setText(path);
        storageIndex = user.getPhotoStorageIndexByPath(path);
        photo = user.getPhoto(storageIndex);

        newCaption.setText(photo.getCaption());
        tempTags = photo.getTag();

        refreshTagNameDrag();
        refreshTagList();

        tempTagList
        .getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			showTagItem(mainStage));

        
        tagNameDrop
        .getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			addTagName(mainStage));
            
    }

    
    /** 
     * @param event
     * @throws Exception
     */
    public void saveChanges(ActionEvent event) throws Exception{
        photo.captionPhoto(newCaption.getText().toString());
        photo.setTags(tempTags);;
        user.setPhoto(storageIndex, photo);
        
        //user.writeUserSession();
        returnToUserMain(event);       
    }

    
    /** 
     * @param event
     * @throws Exception
     */
    public void returnToUserMain(ActionEvent event) throws Exception{
        user.writeUserSession();
        FXMLLoader loader = new FXMLLoader();   
        loader.setLocation(
            getClass().getResource("UserSys.fxml"));
        Parent root = (Parent)loader.load();
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 1116, 538);
        UserSysController userSysController = loader.getController();
        userSysController.continueUserSession(stage);
        stage.setScene(scene);
        stage.show();
    }

    
    /** 
     * @param mainStage
     */
    private void addTagName(Stage mainStage){
        String tagName = user.getTagNameByIndex(tagNameDrop.getSelectionModel().getSelectedIndex());
        if(tagName.equals("Add Your Own...")){
            TextInputDialog tagInput= new TextInputDialog();
            tagInput.initOwner(mainStage); 
            tagInput.setTitle("New Tag Name");
            tagInput.setHeaderText("Add your own tag name");
            Optional<String> result = tagInput.showAndWait();
            if (result.isPresent()){
               user.addTagName(result.get());
               refreshTagNameDrag();
               tagNameDrop.getSelectionModel().clearSelection();
               try{
                    user.writeUserSession();
               }catch(Exception e){
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setContentText("Fail to save data");
                    alert.showAndWait();
               }
               
            }
        }
    }

    private void refreshTagNameDrag(){
        ArrayList<String> keySet = new ArrayList<>(this.user.getTagNames().keySet());
        tagNameDrop.setItems(FXCollections.observableArrayList(keySet));
	}

    private void refreshTagList(){
        ArrayList<String> tagStrings = new ArrayList<String>();
        for(int i=0; i<tempTags.size(); i++){
            tagStrings.add(tempTags.get(i).showTag());
        }
		obsList = FXCollections.observableArrayList(tagStrings);
		tempTagList.setItems(obsList);
	}

    
    /** 
     * @param mainStage
     */
    private void showTagItem(Stage mainStage) {
		int index = tempTagList.getSelectionModel().getSelectedIndex();
		if(index<0){
			return;
		}
		
		tagNameDrop.setValue(tempTags.get(index).getName());
        tagValueField.setText(tempTags.get(index).getValue());
	}

    
    /** 
     * @param event
     */
    public void addTagToTemp(ActionEvent event){
        String tagName = tagNameDrop.getValue().toString();
        String tagValue = tagValueField.getText().toString();
        
        if(tagName.length()==0 || tagValue.length()==0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("You have to input both tag name and tag value");
            alert.showAndWait();
            return;
        }

        for(int i=0; i<tempTags.size(); i++){
            if(tagName.equals(tempTags.get(i).getName()) && tagValue.equals(tempTags.get(i).getValue())){
                Alert alert = new Alert(AlertType.WARNING);
                alert.setContentText("Duplicate tag name and value");
                alert.showAndWait();
                return;
            }
        }

        tempTags.add(new Tag(tagName, tagValue));
        refreshTagList();
    }

    
    /** 
     * @param event
     */
    public void deleteTagFromTemp(ActionEvent event){
        int index = tempTagList.getSelectionModel().getSelectedIndex();
		if(index<0){
			return;
		}
        tempTags.remove(index);
        refreshTagList();
    }
}
