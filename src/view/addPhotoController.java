/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.Node;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;

import app.Tag;
import app.User;

/**
 * controller to add photo
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */

public class addPhotoController {

    @FXML Button addPhoto;
    @FXML Button clear;
    @FXML Button cancel;
    @FXML TextField path;
    @FXML TextArea caption;
    @FXML ListView tempTagList;
    @FXML ChoiceBox tagNameDrop;
    @FXML TextField tagValueField;
    @FXML Button addTag;
    @FXML Button deleteTag;
    @FXML Button selectFile;
    
    String captionToAdd = "";
    ArrayList<Tag> tagsToAdd = new ArrayList<Tag>();//tag list to add
    User user;

    private ObservableList<String> obsList;
    
    
    /** 
     * @param mainStage
     * @throws Exception
     */
    public void start(Stage mainStage) throws Exception{
        user = User.readUserSession();
        refreshTagNameDrag();

        refreshTagList();

        tempTagList
        .getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			showTagItem(mainStage));

        
        tagNameDrop
        .getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			addTagName(mainStage));
            
    }
    
    
    /** 
     * @param event
     * @throws Exception
     */
    public void addPhoto(ActionEvent event) throws Exception{

        if(caption.getText().toString().length()>0){
            captionToAdd = caption.getText().toString();
        }
        if(path.getText().toString().length() == 0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("You must select a photo");
            alert.showAndWait();
            return;
        }

        try{
           user.createPhoto(user.selectedAlbumIndex, path.getText().toString(), captionToAdd, tagsToAdd);
        }catch(RuntimeException e){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("This photo is already in library" );
            alert.showAndWait();
        }
        //user.writeUserSession();
        returnToUserMain(event);      
    }
    
    
    /** 
     * @param event
     * @throws Exception
     */
    public void returnToUserMain(ActionEvent event) throws Exception{
        user.writeUserSession();
        FXMLLoader loader = new FXMLLoader();   
        loader.setLocation(
            getClass().getResource("UserSys.fxml"));
        Parent root = (Parent)loader.load();
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 1116, 538);
        UserSysController userSysController = loader.getController();
        userSysController.continueUserSession(stage);
        stage.setScene(scene);
        stage.show();
    }

    
    /** 
     * @param mainStage
     */
    private void addTagName(Stage mainStage){
        String tagName = user.getTagNameByIndex(tagNameDrop.getSelectionModel().getSelectedIndex());
        System.out.println("tag name: "+tagName);
        if(tagName.equals("Add Your Own...")){
            TextInputDialog tagInput= new TextInputDialog();
            tagInput.initOwner(mainStage); 
            tagInput.setTitle("New Tag Name");
            tagInput.setHeaderText("Add your own tag name");
            Optional<String> result = tagInput.showAndWait();
            if (result.isPresent()){
               user.addTagName(result.get());
               refreshTagNameDrag();
               tagNameDrop.getSelectionModel().clearSelection();
               try{ 
                    user.writeUserSession();
               }catch(Exception e){
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setContentText("Fail to save data");
                    alert.showAndWait();
               }
               return;
            }
        }  
        
    }

    private void refreshTagNameDrag(){
        ArrayList<String> keySet = new ArrayList<>(this.user.getTagNames().keySet());
        tagNameDrop.setItems(FXCollections.observableArrayList(keySet));
	}

    private void refreshTagList(){
        ArrayList<String> tagStrings = new ArrayList<String>();
        for(int i=0; i<tagsToAdd.size(); i++){
            tagStrings.add(tagsToAdd.get(i).showTag());
        }
		obsList = FXCollections.observableArrayList(tagStrings);
		tempTagList.setItems(obsList);
	}

    
    /** 
     * @param mainStage
     */
    private void showTagItem(Stage mainStage) {
		int index = tempTagList.getSelectionModel().getSelectedIndex();
		if(index<0){
			return;
		}
		
		tagNameDrop.setValue(tagsToAdd.get(index).getName());
        tagValueField.setText(tagsToAdd.get(index).getValue());
	}

    
    /** 
     * @param event
     */
    public void addTagToTemp(ActionEvent event){
        String tagName = tagNameDrop.getValue().toString();
        String tagValue = tagValueField.getText().toString();
        
        if(tagName.length()==0 || tagValue.length()==0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("You have to input both tag name and tag value");
            alert.showAndWait();
            return;
        }

        for(int i=0; i<tagsToAdd.size(); i++){
            if(tagName.equals(tagsToAdd.get(i).getName()) && tagValue.equals(tagsToAdd.get(i).getValue())){
                Alert alert = new Alert(AlertType.WARNING);
                alert.setContentText("Duplicate tag name and value");
                alert.showAndWait();
                return;
            }
        }

        tagsToAdd.add(new Tag(tagName, tagValue));
        refreshTagList();
    }

    
    /** 
     * @param event
     */
    public void deleteTagFromTemp(ActionEvent event){
        int index = tempTagList.getSelectionModel().getSelectedIndex();
		if(index<0){
			return;
		}
        tagsToAdd.remove(index);
        refreshTagList();
    }

    
    /** 
     * @param event
     */
    public void chooseFile(ActionEvent event){
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog((Stage)((Node)event.getSource()).getScene().getWindow());
        if (file != null) {
            path.setText(file.getPath().toString());
            return;
        }
        path.setText("");
    }

    
    /** 
     * @param event
     */
    public void clearText(ActionEvent event){
        path.clear();
        caption.clear();
        tempTagList.setItems(FXCollections.observableArrayList(new ArrayList<String>()));
    }

}
