/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.Node;
import javafx.stage.Stage;
import app.User;

/**
 * controller to add album
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */


public class addAlbumController {

    @FXML Button add;
    @FXML Button clear;
    @FXML TextField newAlbumName;
    
    
    /** 
     * @param event
     * @throws Exception
     */
    public void addAlbum(ActionEvent event) throws Exception{
        User user = User.readUserSession();
        try{
            user.createAlbum(newAlbumName.getText().toString());
        }catch(RuntimeException e){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("Album name already exists");
            alert.showAndWait();
        }
        user.writeUserSession();
        returnToUserMain(event);
    }

    
    /** 
     * @param event
     * @throws Exception
     */
    public void returnToUserMain(ActionEvent event) throws Exception{
        FXMLLoader loader = new FXMLLoader();   
        loader.setLocation(
            getClass().getResource("UserSys.fxml"));
        Parent root = (Parent)loader.load();
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 1116, 538);
        UserSysController userSysController = loader.getController();
        userSysController.continueUserSession(stage);
        stage.setScene(scene);
        stage.show();
    }

    
    /** 
     * @param event
     */
    public void clearText(ActionEvent event){
        newAlbumName.clear();
    }
}
