/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/

package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * model for user data structure
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */

public class UserList extends ArrayList<User> implements Serializable{
    static final long serialVersionUID = 1L;

    public static final String storeDir = "data";
    public static final String storeFile = "user.dat";

    private ArrayList<String> usernameList = new ArrayList<String>();

    public UserList(){
        super();
    }

    
    /** 
     * @param username
     * @param password
     * @throws RuntimeException
     */
    public void addUser(String username, String password) throws RuntimeException{
        try{
            User newUser = new User(username, password);
            this.add(newUser);
            this.usernameList.add(newUser.getusername());
            Collections.sort(this);
        }catch(RuntimeException e){
            this.remove(this.size()-1);
            throw new RuntimeException("duplicate User not allowed, addition stopped");
        }
    }
    
    
    /** 
     * @param UserIndex
     * @param username
     * @param password
     */
    public void editUserByIndex(int UserIndex, String username, String password){
        if(UserIndex>=0){
            User temp = this.get(UserIndex);
            deleteUserByIndex(UserIndex);
            try{
                User newUser = new User(username, password);
                this.add(newUser);
                Collections.sort(this);
            }catch(RuntimeException e){
                this.remove(this.size()-1);
                this.add(temp);
                Collections.sort(this);
                throw new RuntimeException("duplicate User not allowed, addition stopped");
            }
        }else{
            return;
        }
    }

    
    /** 
     * @return ArrayList<String>
     */
    public ArrayList<String> getUsernameList(){
        return this.usernameList;
    }

    
    /** 
     * @param UserIndex
     */
    public void deleteUserByIndex(int UserIndex){
        if(UserIndex>=0){
            this.remove(UserIndex);
        }else{
            return;
        }
    }

    
    /** 
     * @param i
     * @return String
     */
    public String showUserInfoByIndex(int i){
        String UserInfo;
        try{
            UserInfo=  "    username: "+ this.get(i).getusername() + 
               "\n    password: " +this.get(i).getpassword();
        }catch(IndexOutOfBoundsException e){
            UserInfo = e.toString();
        }
        return UserInfo;
    }
    
    /** 
     * @param i
     * @return String
     */
    public String getNameByIndex(int i){
        return this.get(i).getusername();
    }

    
    /** 
     * @param username
     * @return int
     */
    public int getIndexByUsername(String username){
        for(int i=0; i<this.size(); i++){
            if(username.equals(this.get(i).getusername())){
                return i;
            }
        }
        return -1;
    }

    
    /** 
     * @param i
     * @return String
     */
    public String getpasswordByIndex(int i){
        return this.get(i).getpassword();
    }
    
    /** 
     * @return ArrayList<String>
     */
    public ArrayList<String> showAllUsers(){
        ArrayList<String> UserInfoList= new ArrayList<String>();
        for(int i=0; i<this.size();i++){
            UserInfoList.add(this.get(i).getusername());
        }
        return UserInfoList;
    }
    
    /** 
     * @return UserList
     */
    /*
    public UserList loadUserListFromCSV(String path) throws IOException{
        String fileIn = path;

        FileReader fileReader = new FileReader(fileIn);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;

        while ((line = bufferedReader.readLine()) != null) {
            //System.out.println(line);
            String[] temp = line.split(" ");
            
            String username = temp[0];
            String password = temp[1];
            this.addUser(username, password);
        }

        bufferedReader.close();
        return this;
    }
    
    public void saveList(String path) throws IOException{
    	try (PrintWriter writer = new PrintWriter(path)) {

    	      StringBuilder sb = new StringBuilder();
    	      for(int i=0; i<this.size();i++){
    	    	  //System.out.println("it is fine until now" + i);
    	    	  sb.append(this.get(i).getusername());
    	    	  sb.append(' ');
    	    	  sb.append(this.get(i).getpassword());
                  if(i<this.size()-1){
                    sb.append('\n');
                  }
    	    	  writer.write(sb.toString());
    	    	  sb.setLength(0);
    	      }

    	} catch (FileNotFoundException e) {
    		System.out.println(e.getMessage());
    	}

    }
    */
    public static UserList readUserData() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
            new FileInputStream(storeDir + File.separator + storeFile));
        UserList userlist = (UserList)ois.readObject();
        return userlist;
    }
    
    /** 
     * @throws IOException
     */
    public void writeUserData() throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(
            new FileOutputStream(storeDir + File.separator+storeFile)
        );
        oos.writeObject(this);
    }
    
}
