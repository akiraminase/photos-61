/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package app;

import java.io.Serializable;

/**
 * model for Tag
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */

public class Tag implements Serializable{
	static final long serialVersionUID = 1L;
	private String name;
	private String value;
	public Tag(String name, String value) {
		this.name = name;
		this.value = value;
		
		
	}

	
	/** 
	 * @return String
	 */
	public String getName() {
		return this.name;
	}
	
	
	/** 
	 * @return String
	 */
	public String getValue(){
		return this.value;
	}

	
	/** 
	 * @return String
	 */
	public String showTag(){
		return this.name+"="+this.value;
	}
	
}
