/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package view;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import app.Album;
import app.Photo;
import app.PhotoLib;
import app.Tag;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.Node;
import javafx.stage.Stage;

import view.loginController;

import app.User;
import app.UserList;

/**
 * controller for user system
 * @author Ang Gao
 * @author Isaac Chun
 */


public class UserSysController implements Serializable{

    @FXML Button newAlbum;
    @FXML Button deleteAlbum;
    @FXML Button renameAlbum;
    @FXML Button newPhoto;
    @FXML Button editPhoto;
    @FXML Button deletePhoto;
    @FXML TextField tags;
    @FXML Button addTags;
    @FXML Button logout;
    @FXML ListView albumListView;
    @FXML ListView photoListView;
    
    @FXML DatePicker startDate;
	@FXML DatePicker endDate;
	@FXML Button search;
	@FXML ComboBox sTag;
	@FXML Button sSearch;
	@FXML ComboBox mTag1;
	@FXML ComboBox mTag2;
	@FXML Button cSearch;
	@FXML Button dSearch;
	@FXML ListView result;
	@FXML TextField sValue;
	@FXML TextField mValue1;
	@FXML TextField mValue2;

    @FXML ImageView photoDisplay;
   // @FXML HBox photoArea;
    @FXML Text photoDetails;
    @FXML Button lastPhoto;
    @FXML Button nextPhoto;

    private String username;
    private UserList userList;
    private ObservableList<String> obsList;
    private ObservableList<Photo> photoList;
    private ObservableList<String> tag_name; 
    
    private User user;
    private int userIndex;
    
    /** 
     * @param mainStage
     */
    public void startUserSession(Stage mainStage){
        //get login information
        try{
            username = loginController.readUserLogin();
        }catch(Exception e){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("failed to fatch user login: "+username);
            alert.showAndWait();
            return;
        }

        //read user list data
        try{
            userList = UserList.readUserData();
            if(! userList.getUsernameList().contains(username)){
                Alert alert = new Alert(AlertType.WARNING);
                alert.setContentText("user "+username+" does not exist");
                alert.showAndWait();
                return;
            }
        }catch(Exception e){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("failed to fatch user data: "+username);
            alert.showAndWait();
            return;
        }

        userIndex = userList.getIndexByUsername(username);
        user = userList.get(userIndex);
        
        //for search
        
        ArrayList<String> keySet = new ArrayList<>(this.user.getTagNames().keySet());
        this.tag_name = FXCollections.observableArrayList(keySet);
        sTag.setItems(this.tag_name);
        mTag1.setItems(this.tag_name);
        mTag2.setItems(this.tag_name);
        
        //System.out.println(sTag.getValue().toString());
        showMainUI(mainStage);
        
    }

    
    /** 
     * @param mainStage
     */
    public void continueUserSession(Stage mainStage) {
        try{
            userList = UserList.readUserData();
        }catch(Exception e){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("failed to fatch user data: "+username);
            alert.showAndWait();
        }
        try{
            user = User.readUserSession();
        }catch(Exception e){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("failed to fatch user session: "+username);
            alert.showAndWait();
        }
        showMainUI(mainStage);
	}

    
    /** 
     * @param mainStage
     */
    private void showMainUI(Stage mainStage){
        refreshAlbumList();
        showPhotoFromSelectedAlbum(mainStage);

        albumListView
		.getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			showPhotoFromSelectedAlbum(mainStage));

        photoListView
        .getSelectionModel()
        .selectedIndexProperty()
        .addListener(
            (obs, oldVal, newVal) ->
            showPhoto(mainStage));
        
        sTag
        .getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			addTagName(mainStage, sTag));
        
        mTag1
        .getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			addTagName(mainStage, mTag1));

        mTag2
        .getSelectionModel()
        .selectedIndexProperty()
        .addListener(
            (obs, oldVal, newVal) ->
            addTagName(mainStage, mTag2));

        mainStage.setOnCloseRequest(event -> {
            save();
        });
        mainStage.close();
    }

    
    /** 
     * @param username
     * @return int
     */
    public int getUserIndex(String username){
        for(int i=0; i<userList.size(); i++){
            if(username.equals(userList.get(i).getusername())){
                return i;
            }
        }
        return -1;
    }

    private void refreshAlbumList(){
		obsList = FXCollections.observableArrayList(user.showAllAlbums());
		albumListView.setItems(obsList);
	}

    
    /** 
     * @param mainStage
     */
    @SuppressWarnings("unchecked")
	private void showPhotoFromSelectedAlbum(Stage mainStage) {
		int index = albumListView.getSelectionModel().getSelectedIndex();
		if(index<0){
			return;
		}
		
		ArrayList<Photo> photos = new ArrayList<Photo>();
		
		ArrayList<String> paths = user.getAlbumByIndex(index).showAllPhotoPaths();
		for(int i = 0; i < paths.size(); i++) {
			ArrayList<Photo> userPhotos = user.getPhotos();
			
			for(int j = 0; j < userPhotos.size(); j++) {
				if(userPhotos.get(j).getPath().equals(paths.get(i))) {
					photos.add(userPhotos.get(j));
				}
			}
		}
		System.out.println(photos);
		
		
		
		photoList = FXCollections.observableArrayList(photos);
            photoListView.setItems(photoList);
            photoListView.setCellFactory(listView -> new ListCell<Photo>() {

                @Override
                public void updateItem(Photo photo, boolean empty) {
                    super.updateItem(photo, empty);
                    if (empty) {
                        setGraphic(null);
                    } 
                    else {
                        HBox hBox = new HBox(5);
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        Image image = new Image(photo.getPath());
                        ImageView imageView = new ImageView();
                        imageView.setImage(image);
                        imageView.setFitHeight(100);
                        imageView.setFitWidth(100);
                        imageView.setSmooth(true);
                        imageView.setPreserveRatio(true);
                        hBox.getChildren().addAll(imageView, new Label(photo.getCaption()));


                        setGraphic(hBox);

                    }
                }
            });
	}


    
    /** 
     * @param event
     * @throws Exception
     */
    public void switchToAddAlbum(ActionEvent event) throws Exception{
        user.writeUserSession();
        simpleSwitchScene(event, "addAlbum.fxml");
    }

    
    /** 
     * @param event
     * @throws Exception
     */
    public void switchToRenameAlbum(ActionEvent event) throws Exception{
        int index = albumListView.getSelectionModel().getSelectedIndex();
        if(index<0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("No Album Selected");
            alert.showAndWait();
            return;
        }
        user.selectedAlbumIndex = index;
        user.writeUserSession();
        simpleSwitchScene(event, "renameAlbum.fxml");
    }
    
    
    /** 
     * @param event
     */
    public void deleteAlbum(ActionEvent event){
        int index = albumListView.getSelectionModel().getSelectedIndex();
        if(index<0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("No Album Selected");
            alert.showAndWait();
            return;
        }
        
        Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this album?\n"+ 
									 user.getAlbumByIndex(index).getName(),  ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            user.deleteAlbumByIndex(index);
            photoListView.setItems(FXCollections.observableArrayList(new ArrayList<String>()));
        }
        
        refreshAlbumList();
        photoListView.getSelectionModel().clearSelection();
    }
    
    /** 
     * @param event
     * @throws Exception
     */
    public void switchToAddPhoto(ActionEvent event) throws Exception{
        int index = albumListView.getSelectionModel().getSelectedIndex();
        if(index<0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("No Album Selected");
            alert.showAndWait();
            return;
        }
        user.selectedAlbumIndex = index;
        user.writeUserSession();
        
        FXMLLoader loader = new FXMLLoader();   
        loader.setLocation(
            getClass().getResource("addPhoto.fxml"));
        Parent root = (Parent)loader.load();
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 500);
        addPhotoController addPhotoController = loader.getController();
        addPhotoController.start(stage);
        stage.setScene(scene);
        stage.show();
    }

    
    /** 
     * @param event
     * @throws Exception
     */
    public void switchToEditPhoto(ActionEvent event) throws Exception{
        int albumIndex = albumListView.getSelectionModel().getSelectedIndex();
        if(albumIndex<0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("No Album Selected");
            alert.showAndWait();
            return;
        }
        user.selectedAlbumIndex = albumIndex;

        int photoIndex = photoListView.getSelectionModel().getSelectedIndex();
        if(photoIndex<0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("No Photo Selected");
            alert.showAndWait();
            return;
        }
        user.selectedPhotoIndex = photoIndex;

        user.writeUserSession();
        
        FXMLLoader loader = new FXMLLoader();   
        loader.setLocation(
            getClass().getResource("editPhoto.fxml"));
        Parent root = (Parent)loader.load();
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 500);
        editPhotoController editPhotoController = loader.getController();
        editPhotoController.start(stage);
        stage.setScene(scene);
        stage.show();
    }

    
    /** 
     * @param event
     */
    public void deletePhotoFromAlbum(ActionEvent event){
        int index = photoListView.getSelectionModel().getSelectedIndex();
        if(index<0){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("No photo Selected");
            alert.showAndWait();
            return;
        }
        
        Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this photo from this album?\n"+ 
									 user.getAlbumByIndex(user.selectedAlbumIndex).getPhotoPathByIndex(index),  ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            user.getAlbumByIndex(user.selectedAlbumIndex).removePhotoPathByIndex(index);
        }
        
        showPhotoFromSelectedAlbum((Stage)((Node)event.getSource()).getScene().getWindow());
        photoListView.getSelectionModel().clearSelection();
    }
    
    /** 
     * @param event
     */
    public void addTagFilter(ActionEvent event){
        //TODO: implement photo tag filter
    }
    
    
    /** 
     * @param event
     * @throws Exception
     */
    public void userLogout(ActionEvent event) throws Exception{
        save();
		PhotoLib photolib = new PhotoLib();
		photolib.start((Stage)((Node)event.getSource()).getScene().getWindow());
    }

    private void save(){
        //delete non albumed photos
        for(int i=0; i<user.getPhotosLength(); i++){
            if(PhotoisPresentInAlbum(user.getPhoto(i)) == -1){
                user.deletePhotoStorageByIndex(i);
                i--;
            }
        }
        try{
            userList.set(userIndex, user);
            userList.writeUserData();
        }catch(Exception e){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setContentText("Failed to save data: "+e);
            alert.showAndWait();
        }
    }

    
    /** 
     * @param p
     * @return int
     */
    public int PhotoisPresentInAlbum(Photo p){
        if(user.getAlbumsLength() == 0){
            return -1;
        }
        for(int i=0; i<user.getAlbumsLength(); i++){
            Album album = user.getAlbumByIndex(i);
            for(int j=0; j<album.getPhotoPathLength();j++){
                if(user.getAlbumByIndex(i).getPhotoPathByIndex(j).equals(p.getPath())){
                    return i;
                }
            }
        }
        return -1;
    }
   // public void filterByDate(ActionEvent event) throws Exception{
    

    
    public void searchByDate(ActionEvent event) throws Exception{
        //TODO: implement photo date filter
    	try {
			LocalDate sd = startDate.getValue();
    		LocalDate ed = endDate.getValue();
    		if(sd.compareTo(ed) > 0) {
    			Alert alert = new Alert(AlertType.ERROR);
        		alert.setContentText("Invalid Date");
        		alert.showAndWait();
        		return;
    		}
    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    		String startDate = sd.format(formatter);
    		String endDate = ed.format(formatter);
    		
    		System.out.println(startDate + " " + endDate);
    		
    		ArrayList<Photo> result = this.user.searchByDateRange(startDate, endDate);
    		System.out.println(result);
    		
    		user.writeUserSession();
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("searchResult.fxml")); 
    		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            Parent root = (Parent)loader.load();
            Scene scene = new Scene(root, 621, 428);
            searchResultController controller = loader.getController();
            controller.initData(result);
            stage.setScene(scene);
            stage.show();
            
            
            
		}
		catch(Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Invalid Date");
    		alert.showAndWait();
		}
    }
    
    
    /** 
     * @param mainStage
     * @param drop
     */
    private void addTagName(Stage mainStage, ComboBox drop){
        String tagName = user.getTagNameByIndex(drop.getSelectionModel().getSelectedIndex());
        System.out.println("tag name: "+tagName);
        if(tagName.equals("Add Your Own...")){
            TextInputDialog tagInput= new TextInputDialog();
            //tagInput.initOwner(mainStage); 
            tagInput.setTitle("New Tag Name");
            tagInput.setHeaderText("Add your own tag name");
            Optional<String> result = tagInput.showAndWait();
            drop.valueProperty().set(null);
            if (result.isPresent()){
               user.addTagName(result.get());
               refreshTagNameDrag(drop);
               return;
            }
        }  
        
    }
    
    /** 
     * @param drop
     */
    private void refreshTagNameDrag(ComboBox drop){
        ArrayList<String> keySet = new ArrayList<>(this.user.getTagNames().keySet());
        drop.setItems(FXCollections.observableArrayList(keySet));
	}

    
    /** 
     * @param event
     * @throws Exception
     */
    public void sSearch(ActionEvent event) throws Exception{
		try {
			String name = sTag.getValue().toString();
			String value = sValue.getText();
			
			ArrayList<Photo> result = this.user.searchBySingle(name, value);
			System.out.println(result);
			
			user.writeUserSession();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("searchResult.fxml")); 
    		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            Parent root = (Parent)loader.load();
            Scene scene = new Scene(root, 621, 428);
            searchResultController controller = loader.getController();
            controller.initData(result);
            stage.setScene(scene);
            stage.show();
		}
    	catch(Exception e) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Invalid Value");
    		alert.showAndWait();
    	}
		
	}
    
    
    /** 
     * @param event
     * @throws Exception
     */
    public void cSearch(ActionEvent event) throws Exception{
    	try {
			String name1 = mTag1.getValue().toString();
			String value1 = mValue1.getText();
			
			String name2 = mTag2.getValue().toString();
			String value2 = mValue2.getText();
			
			if(name1.equals(name2) && this.user.getTagNames().get(name1) == false) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Single Value Tag Type cannot appear on both arms");
	    		alert.showAndWait();
	    		return;
			}
			
			ArrayList<Photo> result = this.user.searchByConjuctive(name1, value1, name2, value2);
			System.out.println(result);
			
			user.writeUserSession();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("searchResult.fxml")); 
    		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            Parent root = (Parent)loader.load();
            Scene scene = new Scene(root, 621, 428);
            searchResultController controller = loader.getController();
            controller.initData(result);
            stage.setScene(scene);
            stage.show();
		}
    	catch(Exception e) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Invalid Value");
    		alert.showAndWait();
    	}
	}
    
    
    /** 
     * @param event
     * @throws Exception
     */
    public void dSearch(ActionEvent event) throws Exception{
    	try {
			String name1 = mTag1.getValue().toString();
			String value1 = mValue1.getText();
			
			String name2 = mTag2.getValue().toString();
			String value2 = mValue2.getText();
			
			if(name1.equals(name2) && this.user.getTagNames().get(name1) == false) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Single Value Tag Type cannot appear on both arms");
	    		alert.showAndWait();
	    		return;
			}
			
			ArrayList<Photo> result = this.user.searchByDisjunctive(name1, value1, name2, value2);
			System.out.println(result);
			
			
			user.writeUserSession();
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("searchResult.fxml")); 
    		Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            Parent root = (Parent)loader.load();
            Scene scene = new Scene(root, 621, 428);
            searchResultController controller = loader.getController();
            controller.initData(result);
            stage.setScene(scene);
            stage.show();
		}
    	catch(Exception e) {
    		Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Invalid Value");
    		alert.showAndWait();
    	}
	}
    
//    public void addAlbum(ActionEvent event) throws Exception{
//		
//	}
    
    private void showPhoto(Stage mainStage){
        int index = photoListView.getSelectionModel().getSelectedIndex();
        String path = user.getAlbumByIndex(user.selectedAlbumIndex).getPhotoPathByIndex(index);
        Image image = new Image(path);
       // ImageView photoDisplay = new ImageView();
        photoDisplay.setImage(image);
        //photoDisplay.setFitHeight(400);
        //photoDisplay.setFitWidth(300);
        photoDisplay.setSmooth(true);
        photoDisplay.setPreserveRatio(true);
        Photo photo = user.getPhoto(user.getPhotoStorageIndexByPath(path));


        String details = "Caption: \n"+photo.getCaption() + "\n\nDate: \n" + photo.getModi()+"\n\nTags: \n";
        ArrayList<Tag> displayTags = photo.getTag();
        for(int i=0; i<displayTags.size(); i++){
            details = details+displayTags.get(i).showTag()+"\s";
            if(i%3==0){
                details = details+"\n";
            }
        }
		photoDetails.setText(details);
    }
    
    
    /** 
     * @param event
     */
    public void switchtoLastPhoto(ActionEvent event){
        photoListView.getSelectionModel().selectPrevious();
    }

    
    /** 
     * @param event
     */
    public void switchtoNextPhoto(ActionEvent event){
        //if(user.selectedPhotoIndex+1>=user.getAlbumByIndex(user.selectedAlbumIndex).getPhotoPathLength()){
        //    return;
        //}
        photoListView.getSelectionModel().selectNext();
    }
    
    /** 
     * @param event
     * @param fxml
     * @throws Exception
     */
    //helper method
    public void simpleSwitchScene(ActionEvent event, String fxml) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource(fxml));
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 600, 500);
        stage.setScene(scene);
        stage.show();
    }

}
