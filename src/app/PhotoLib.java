/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/

package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Parent;

/**
 * model for the main launching interface
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */

public class PhotoLib extends Application {
	static final long serialVersionUID = 1L;

	
	/** 
	 * @param primaryStage
	 * @throws Exception
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {

		Parent root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
	
		Scene scene = new Scene(root, 300, 200);
		primaryStage.setTitle("Photo Library");
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show(); 
	}

	
	/** 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
