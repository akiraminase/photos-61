/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package view;

import java.util.ArrayList;

import app.Album;
import app.Photo;
import app.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * controller for search result
 * @author Ang Gao
 * @author Isaac Chun
 */

public class searchResultController {
	@FXML Button addAlbum;
	@FXML Button returnMain;
	@FXML TextField t1;
	@FXML ListView resultView;
	
	private ArrayList<String> paths = new ArrayList<String>();
	private ObservableList<Photo> obsList;
	
	
	
    /** 
     * @param results
     */
    @SuppressWarnings("unchecked")
	public void initData(ArrayList<Photo> results) {
		
		for(int i = 0; i < results.size(); i++) {
			this.paths.add(results.get(i).getPath());
		}
				
		obsList = FXCollections.observableArrayList(results);
		resultView.setItems(obsList);
        resultView.setCellFactory(listView -> new ListCell<Photo>() {

            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);
                if (empty) {
                    setGraphic(null);
                } 
                else {
                    HBox hBox = new HBox(5);
                    hBox.setAlignment(Pos.CENTER_LEFT);
                    Image image = new Image(photo.getPath());
                    ImageView imageView = new ImageView();
                    imageView.setImage(image);
                    imageView.setFitHeight(100);
                    imageView.setFitWidth(100);
                    imageView.setSmooth(true);
                    imageView.setPreserveRatio(true);
                    hBox.getChildren().addAll(imageView, new Label(photo.getCaption()));


                    setGraphic(hBox);

                }
            }
        });
        
	}
	
	
    /** 
     * @param event
     * @throws Exception
     */
    public void addAlbum(ActionEvent event) throws Exception{
		
		User user = User.readUserSession();
	 	try{
	 		if(t1.getText().toString().length() == 0) {
	 			Alert alert = new Alert(AlertType.WARNING);
	            alert.setContentText("Album can't be length 0");
	            alert.showAndWait();
	            return;
	 		}
	 		
            user.createAlbum(t1.getText().toString());
            Album album = user.getAlbumByName(t1.getText().toString());
            album.setPhotoPaths(this.paths);
            user.writeUserSession();
            returnToUserMain(event);
            
        }catch(RuntimeException e){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setContentText("Album name already exists");
            alert.showAndWait();
        }
	}
	
	
	
    /** 
     * @param event
     * @throws Exception
     */
    public void returnToUserMain(ActionEvent event) throws Exception{
        FXMLLoader loader = new FXMLLoader();   
        loader.setLocation(
            getClass().getResource("UserSys.fxml"));
        Parent root = (Parent)loader.load();
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        Scene scene = new Scene(root, 1116, 538);
        UserSysController userSysController = loader.getController();
        userSysController.continueUserSession(stage);
        stage.setScene(scene);
        stage.show();
    }
}
