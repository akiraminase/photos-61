/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package app;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * model for album
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */
public class Album implements Serializable{
	static final long serialVersionUID = 1L;
	
	private ArrayList<String> photoPaths = new ArrayList<String>();
	private String name;
	
	public Album(String name) {
		this.name = name;
	}
	
	
	/** 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/** 
	 * @param paths
	 */
	public void setPhotoPaths(ArrayList<String> paths) {
		this.photoPaths = paths;
	}
	
	
	/** 
	 * @return String
	 */
	public String getName() {
		return this.name;
	}
	
	
	/** 
	 * @param path
	 */
	public void addPhotoPath(String path) {
		this.photoPaths.add(path);
	}

	
	/** 
	 * @param i
	 * @return String
	 */
	public String getPhotoPathByIndex(int i) {
		return this.photoPaths.get(i);
	}
	
	
	/** 
	 * @return int
	 */
	public int getPhotoPathLength() {
		return this.photoPaths.size();
	}
	

	
	/** 
	 * @param path
	 */
	public void removePhotoPath(String path) {
		this.photoPaths.remove(path);
	}

	
	/** 
	 * @param i
	 */
	public void removePhotoPathByIndex(int i) {
		this.photoPaths.remove(i);
	}

    
	/** 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> showAllPhotoPaths(){
        ArrayList<String> photoList= new ArrayList<String>();
        for(int i=0; i<this.photoPaths.size();i++){
			System.out.println(this.photoPaths.get(i));
            photoList.add(this.photoPaths.get(i));
        }
        return photoList;
    }
	
	
}

