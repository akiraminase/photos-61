/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.io.ObjectOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javafx.event.ActionEvent;

/**
 * controller to login
 * @author Ang Gao
 * @author Isaac Chun
 */

public class loginController {

    @FXML Button login;
    @FXML Button clear;
    @FXML TextField username;
    @FXML PasswordField password;

    public static final String storeDir = "data";
    public static final String storeFile = "currentLogin.dat";

    
    /** 
     * @param event
     * @throws Exception
     */
    public void userLogin(ActionEvent event) throws Exception{
        if(username.getText().toString().equals("admin") && password.getText().toString().equals("admin")) {
            FXMLLoader loader = new FXMLLoader();   
		    loader.setLocation(
				getClass().getResource("AdminSys.fxml"));
            Parent root = (Parent)loader.load();
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root, 600, 500);
            AdminSysController adminSysController = loader.getController();
		    adminSysController.start(stage);
            stage.setScene(scene);
            stage.show();
        }else if(username.getText().isEmpty() || password.getText().isEmpty()) {
            Alert alert = new Alert(AlertType.WARNING, "Please enter your credentials.");
            alert.showAndWait();
        }else {
            //TODO: add user verification
            writeUserLogin(username.getText().toString());
            FXMLLoader loader = new FXMLLoader();   
		    loader.setLocation(
				getClass().getResource("UserSys.fxml"));
            Parent root = (Parent)loader.load();
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root, 1116, 538);
            UserSysController userSysController = loader.getController();
		    userSysController.startUserSession(stage);
            stage.setScene(scene);
            stage.show();
        }
    }

    
    /** 
     * @return String
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static String readUserLogin() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
            new FileInputStream(storeDir + File.separator + storeFile));
            String userlist = (String)ois.readObject();
        return userlist;
    }
    
    /** 
     * @param username
     * @throws IOException
     */
    public static void writeUserLogin(String username) throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(
            new FileOutputStream(storeDir + File.separator + storeFile)
        );
        oos.writeObject(username);
    }

    
    /** 
     * @param event
     */
    public void clearText(ActionEvent event){
        username.clear();
        password.clear();
    }
}