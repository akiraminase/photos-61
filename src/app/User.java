/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/

package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.Photo;

/**
 * model for User
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */
public class User implements Comparable<User>, Serializable{
    static final long serialVersionUID = 1L;
    private String username;
    private String password;
//    private ArrayList<String> tag_names;
    private HashMap<String, Boolean> tag_names;
    private ArrayList<Album> albums;
    private ArrayList<Photo> photos;

    public int selectedAlbumIndex = -1;//selected index from UI mapping to albums[i]
    public int selectedPhotoIndex = -1;//selected index from UI mapping to albums[i].photoPaths[j]

    public static final String storeDir = "data";
    public static final String storeFile = "currentSession.dat";
    
    public User(String username, String password){
        this.username = username;
        this.password = password;
        this.tag_names = new HashMap<String, Boolean>();
        this.albums  = new ArrayList<Album>();
        this.photos  = new ArrayList<Photo>();

        //default list of tag names
        tag_names.put("location", false);
        tag_names.put("person", true);
        tag_names.put("occasion", true);
        tag_names.put("mood", true);
        tag_names.put("purpose",true);
        tag_names.put("Add Your Own...",false);
    }
    
    /** 
     * @return String
     */
    public String getusername(){
        return username;
    }

    
    /** 
     * @return String
     */
    public String getpassword(){
        return password;
    }

    
    /** 
     * @param i
     * @return String
     */
    public String getTagNameByIndex(int i){
        ArrayList<String> keySet = new ArrayList<>(this.getTagNames().keySet());
        return keySet.get(i);
    }

    
    /** 
     * @param i
     * @return Boolean
     */
    public Boolean getTagBooleanByIndex(int i){
        ArrayList<String> keySet = new ArrayList<>(this.getTagNames().keySet());

        return tag_names.get(keySet.get(i));
    }

    
    /** 
     * @param i
     * @return Album
     */
    public Album getAlbumByIndex(int i){
        return this.albums.get(i);
    }
    
    /** 
     * @return int
     */
    public int getAlbumsLength(){
        return this.albums.size();
    }
    
    
    /** 
     * @param name
     * @return Album
     */
    public Album getAlbumByName(String name){
    	for(int i = 0; i < this.albums.size(); i++) {
    		if(name.equals(albums.get(i).getName())) {
    			return albums.get(i);
    		}
    	}
    	return null;
    }
    
    
    /** 
     * @return ArrayList<Photo>
     */
    public ArrayList<Photo> getPhotos(){
    	return this.photos;
    }
    
    
    /** 
     * @param path
     * @return int
     */
    public int getPhotoStorageIndexByPath(String path){
        for(int i=0; i<this.photos.size(); i++){
            if(path.equals(this.photos.get(i).getPath())){
                return i;
            }
        }
        return -1;
    }
    
    /** 
     * @param storageIndex
     * @param p
     */
    public void setPhoto(int storageIndex, Photo p){
        this.photos.set(storageIndex, p);
    }

    
    /** 
     * @param storageIndex
     * @return Photo
     */
    public Photo getPhoto(int storageIndex){
        return this.photos.get(storageIndex);
    }

    
    /** 
     * @param username
     */
    public void setusername(String username){
        this.username = username;
    }

    
    /** 
     * @param password
     */
    public void setpassword(String password){
        this.password = password;
    }

    
    /** 
     * @param tag_Name
     */
    public void addTagName(String tag_Name){
    	//TODO multivalue option
        this.tag_names.put(tag_Name, true);
    }

    
    /** 
     * @return HashMap<String, Boolean>
     */
    public HashMap<String,Boolean> getTagNames(){
        return this.tag_names;
    }

    
    /** 
     * @return int
     */
    public int getTagNamesLength(){
        return this.tag_names.size();
    }

    /*
    public void deleteTagTypeByIndex(int i){
        tag_names.remove(i);
    }
    */
    public void deleteAlbumByIndex(int i){
        this.albums.remove(i);
    }
    
    
    /** 
     * @param name
     * @throws RuntimeException
     */
    public void createAlbum(String name) throws RuntimeException{
    	//you can't create album of same name
    	for(int i = 0; i < this.albums.size(); i++) {
    		if(this.albums.get(i).getName().equals(name)){
    			throw new RuntimeException("duplicate album!");
            }
    	}
    	
    	Album a = new Album(name);
    	this.albums.add(a);
    }
    
    
    /** 
     * @param name
     * @param index
     * @throws RuntimeException
     */
    public void renameAlbumByIndex(String name, int index) throws RuntimeException{
    	//you can't create album of same name
    	for(int i = 0; i < this.albums.size(); i++) {
    		if(this.albums.get(i).getName().equals(name)){
    			throw new RuntimeException("duplicate album name!");
            }
    	}
    	this.albums.get(index).setName(name);
    }

    
    /** 
     * @param selectedAlbumIndex
     * @param path
     * @param caption
     * @param tags
     */
    //i is the index of current album
    public void createPhoto(int selectedAlbumIndex, String path, String caption, ArrayList<Tag> tags) {
        //check path in storage arraylist
        for(int i = 0; i < this.photos.size(); i++) {
    		if(this.photos.get(i).getPath().equals(path)){
    			throw new RuntimeException("duplicate photo under current user!");
            }
    	}
        //check path in album
        for(int i = 0; i < this.albums.get(selectedAlbumIndex).getPhotoPathLength() ; i++) {
    		if(this.albums.get(selectedAlbumIndex).getPhotoPathByIndex(i).equals(path)){
    			throw new RuntimeException("duplicate photo under current album!");
            }
            
    	}

    	Photo p = new Photo(path, caption, tags);
        this.photos.add(p);
    	this.albums.get(selectedAlbumIndex).addPhotoPath(path);
        System.out.println(path+" added");
    }

    
    /** 
     * @param i
     */
    public void deletePhotoStorageByIndex(int i){
        System.out.println(this.photos.get(i).getPath()+" is removed");
        this.photos.remove(i);
    }

    
    /** 
     * @return int
     */
    public int getPhotosLength(){
        return this.photos.size();
    }
    
    /** 
     * @param i
     * @param p
     * @return ArrayList<String>
     */
    /*
    //i is the index of designation album
    public void copyPhoto(int i, Photo p) {
    	this.albums.get(i).addPhoto(p);
    }
    //might need new scene
    
    //s is index of start album, e is the index of end album
    public void movePhoto(int s, int e, Photo p) {
    	this.albums.get(s).removePhoto(p);
    	this.albums.get(e).addPhoto(p);
    }
    //might need new scene
    */
    public ArrayList<String> showAllAlbums(){
        ArrayList<String> albumList= new ArrayList<String>();
        for(int i=0; i<this.albums.size();i++){
            albumList.add(this.albums.get(i).getName());
        }
        return albumList;
    }
    
    
    
    /** 
     * @param s
     * @return boolean
     */
    //Helper method
    public boolean validDate(String s) {
    	SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
    	f.setLenient(false);
    	try {
    		f.parse(s.trim());
    	} 
    	catch(ParseException e) {
    		return false;
    	}
    	
    	return true;
    }
    
    
    
    /** 
     * @param dateStart
     * @param dateEnd
     * @return ArrayList<Photo>
     * @throws Exception
     */
    //TODO add alarm for case 1. parameter format is wrong, 2. 
    public ArrayList<Photo> searchByDateRange(String dateStart, String dateEnd) throws Exception{
    	ArrayList<Photo> searchPhoto = new ArrayList<Photo>();
    	if(validDate(dateStart) && validDate(dateEnd)) {
			Date dStart = new SimpleDateFormat("MM/dd/yyyy").parse(dateStart);
			Date dEnd = new SimpleDateFormat("MM/dd/yyyy").parse(dateEnd);
			
			for(int i = 0; i < this.photos.size(); i++) {
				Photo p = this.photos.get(i);
				Date pDate = new SimpleDateFormat("MM/dd/yyyy").parse(p.getModi());
					
				//if date is in between check if this object already exists in returning arraylist
				if(pDate.compareTo(dStart) >= 0 && pDate.compareTo(dEnd) <= 0) {
					if(!searchPhoto.contains(p)) searchPhoto.add(p);
				}
			}
    	}
    	
    	return searchPhoto;
    }
    
    
    
    /** 
     * @param name
     * @param value
     * @return ArrayList<Photo>
     */
    //TODO search by single tag type value pair
    public ArrayList<Photo> searchBySingle(String name, String value) {
    	ArrayList<Photo> searchPhoto = new ArrayList<Photo>();
    	for(int i = 0; i < this.photos.size(); i++) {
    		Photo p = this.photos.get(i);
    		ArrayList<Tag> pTags = p.getTag();
    		
    		for(int j = 0; j < pTags.size(); j++) {
    			if(pTags.get(j).getName().equals(name) && pTags.get(j).getValue().equals(value)) {
    				searchPhoto.add(p);
    				break;
    			}
    		}
    	}
    	
    	
    	return searchPhoto;
    }
    
    
    
    /** 
     * @param name1
     * @param value1
     * @param name2
     * @param value2
     * @return ArrayList<Photo>
     */
    public ArrayList<Photo> searchByConjuctive(String name1, String value1, String name2, String value2) {
    	ArrayList<Photo> searchPhoto = new ArrayList<Photo>();
    	for(int i = 0; i < this.photos.size(); i++) {
    		Photo p = this.photos.get(i);
    		ArrayList<Tag> pTags = p.getTag();
    		boolean aBool = false;
    		boolean bBool = false;
    		
    		for(int j = 0; j < pTags.size(); j++) {
    			Tag pTag = pTags.get(j);
    			if(pTag.getName().equals(name1) && pTag.getValue().equals(value1)) {
    				aBool = true;
    			}
    			else if(pTag.getName().equals(name2) && pTag.getValue().equals(value2)) {
    				bBool = true;
    			}
    		}
    		
    		if(aBool == true && bBool == true) searchPhoto.add(p);
    		
    	}
    	
    	return searchPhoto;
    }
    
    
    
    /** 
     * @param name1
     * @param value1
     * @param name2
     * @param value2
     * @return ArrayList<Photo>
     */
    public ArrayList<Photo> searchByDisjunctive(String name1, String value1, String name2, String value2) {
    	ArrayList<Photo> searchPhoto = new ArrayList<Photo>();
    	for(int i = 0; i < this.photos.size(); i++) {
    		Photo p = this.photos.get(i);
    		ArrayList<Tag> pTags = p.getTag();
    		for(int j = 0; j < pTags.size(); j++) {
    			Tag pTag = pTags.get(j);
    			if(pTag.getName().equals(name1) && pTag.getValue().equals(value1)) {
    				searchPhoto.add(p);
    				break;
    			}
    			
    			else if(pTag.getName().equals(name2) && pTag.getValue().equals(value2)) {
    				searchPhoto.add(p);
    				break;
    			}
    		}
    	}
    	
    	return searchPhoto;
    }
    
    
    /** 
     * @param s
     * @return int
     * @throws RuntimeException
     */
    //TODO create album for search result

    @Override
    public int compareTo(User s) throws RuntimeException{
        int ret = this.username.compareToIgnoreCase(s.username);
        //if usernames are the same
        if(ret == 0){
            throw new RuntimeException("duplicate User");
        }
        return ret;
    }

    
    /** 
     * @return User
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static User readUserSession() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
        new FileInputStream(storeDir + File.separator + storeFile));
        User user = (User)ois.readObject();
        return user;
    }
    
    /** 
     * @throws IOException
     */
    public void writeUserSession() throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(
            new FileOutputStream(storeDir + File.separator + storeFile)
        );
        oos.writeObject(this);
    }
}
