/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/
package app;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import app.Tag;

/**
 * model for photo
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */

public class Photo implements Serializable{
	static final long serialVersionUID = 1L;
	
	private ArrayList<Tag> tags = new ArrayList<Tag>(); //user input of tags
	private File file;
	private String path; // act as primary key to identify uniqueness
	private String modi_date;
	private String caption;
	
	
	public Photo(String path, String caption, ArrayList<Tag> tags) {
		this.path = path;
		this.file = new File(path);
		this.caption = caption;
		this.tags = tags;
		long ftime = file.lastModified();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(ftime);
		c.set(Calendar.MILLISECOND, 0);
		this.modi_date = df.format(c.getTime());
		System.out.println(modi_date);
	}
	
	
	/** 
	 * @return String
	 */
	public String getPath(){
		return this.path;
	}
	
	/** 
	 * @param caption
	 */
	public void captionPhoto(String caption) {
		this.caption = caption;
	}
	
	
	/** 
	 * @param name
	 * @param value
	 */
	public void addTag(String name, String value){
		this.tags.add(new Tag(name, value));
	}

	
	/** 
	 * @param tags
	 */
	public void setTags(ArrayList<Tag> tags){

	}
	
	/** 
	 * @return String
	 */
	public String getCaption(){
		return this.caption;
	}

	
	/** 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getStringTagList(){
		ArrayList<String> tagStrings = new ArrayList<String>();
        for(int i=0; i<this.tags.size(); i++){
            tagStrings.add(this.tags.get(i).showTag());
        }
		return tagStrings;
	}
	
	
	/** 
	 * @return String
	 */
	public String getModi() {
		return this.modi_date;
	}
	
	
	/** 
	 * @return ArrayList<Tag>
	 */
	public ArrayList<Tag> getTag() {
		return this.tags;
	}
	//will need to rewrite this after new interface discussed
	/*
	public boolean addTag(String name, String value, String tag_types) {
		ArrayList<String> token = new ArrayList<String>();
		String[] to = value.split(",");
		for(int i = 0; i < to.length; i++) {
			token.add(to[i].trim());
		}
		
		
		//check if name and values equal, if so then return false
		for(int i = 0; i < this.tags.size(); i++) {
			Tag temp = this.tags.get(i);
			if(temp.getName().equals(name)) {
				if(temp.getValue().containsAll(token)) {
					return false;
				}
			}
		}
		
		//if tag type exists in hashMap and length of this tag matches token's length then duplicate thus return false
		if(tag_types.containsKey(name) && tag_types.get(name) == token.size()){
			return false;
		}
		
		Tag t = new Tag(name, token);
		this.tags.add(t);
		return true;
	}
	
	public void deleteTag(Tag t) {
		this.tags.remove(t);
	}
*/
	
}
