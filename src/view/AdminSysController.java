/*
	Name: Ang Gao 	  NetID: ag1481
	Name: Isaac Chun  NetID: ikc9
*/

package view;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.Node;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import app.PhotoLib;
import app.UserList;

/**
 * controller for admin system
 * 
 * @author Ang Gao
 * @author Isaac Chun
 */

public class AdminSysController{

	@FXML ListView<String> listView;
	@FXML Button add;
	@FXML Button clear;
	@FXML Button delete;
	@FXML Button logout;
	@FXML Label userDetails;
	@FXML TextField newUsername;
	@FXML TextField newPassword;

	private ObservableList<String> obsList;
	private UserList ul = new UserList();

	
	/** 
	 * @param mainStage
	 */
	public void start(Stage mainStage){

		try{
		 	ul = UserList.readUserData();
		}catch(Exception e){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setContentText("Failed to read data: "+e);
			alert.showAndWait();
		}

		refreshList();
		showItem(mainStage);

		listView
		.getSelectionModel()
		.selectedIndexProperty()
		.addListener(
			(obs, oldVal, newVal) ->
			showItem(mainStage));
		
		add.setOnAction(addEvent(mainStage));
		delete.setOnAction(deleteEvent(mainStage));

		mainStage.setOnCloseRequest(event -> {
			try{
			 	ul.writeUserData();
			}catch(IOException e){
				Alert alert = new Alert(AlertType.ERROR);
				alert.setContentText("Failed to save data: "+e);
				alert.showAndWait();
			}
		});
		mainStage.close();
	}

	
	/** 
	 * @param event
	 * @throws Exception
	 */
	public void adminLogout(ActionEvent event) throws Exception{
		try{
			ul.writeUserData();
	   	}catch(IOException e){
		   Alert alert = new Alert(AlertType.ERROR);
		   alert.setContentText("Failed to save data: "+e);
		   alert.showAndWait();
	   	}
		PhotoLib photolib = new PhotoLib();
		photolib.start((Stage)((Node)event.getSource()).getScene().getWindow());
	}
	
	private void refreshList(){
		obsList = FXCollections.observableArrayList(ul.showAllUsers());
		listView.setItems(obsList);
	}
	
	/** 
	 * @param mainStage
	 * @return EventHandler<ActionEvent>
	 */
	private EventHandler<ActionEvent> addEvent(Stage mainStage){
		EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e)
			{
				String username = newUsername.getText().trim();
				String password = newPassword.getText().trim();

				//TODO: add username and password requirements
				
				if(username.length()>=4 && password.length()>=4){
					try{
					 ul.addUser(username, password);
					}catch(RuntimeException ex){
						Alert alert = new Alert(AlertType.ERROR);
						alert.setContentText("Duplicate User. Adding Action Canceled.");
						alert.showAndWait();
					}
					
				}else{
					Alert alert = new Alert(AlertType.WARNING);
					alert.setContentText("Both username and password need to be longer than 4 characters");
					alert.showAndWait();
				}
				
				refreshList();
				showItem(mainStage);

				newUsername.clear();
				newPassword.clear();
			}
		};
		return event;
	}

	
	
	/** 
	 * @param mainStage
	 * @return EventHandler<ActionEvent>
	 */
	private EventHandler<ActionEvent> deleteEvent(Stage mainStage){
		EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e)
			{	
				int index = listView.getSelectionModel().getSelectedIndex();
				if(index == -1){
					Alert alert = new Alert(AlertType.WARNING, "No user Selected");
					alert.showAndWait();
					return;
				}
				
				Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this user?\n\n"+ 
									 ul.showUserInfoByIndex(index),  ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
				alert.showAndWait();
				if (alert.getResult() == ButtonType.YES) {
				 ul.deleteUserByIndex(index);
				}
				refreshList();
				showItem(mainStage);
			}
		};
		return event;
	}

	
	/** 
	 * @param mainStage
	 */
	private void showItem(Stage mainStage) {
		int index = listView.getSelectionModel().getSelectedIndex();
		if(index<0){
			userDetails.setText("    No User Selected");
			return;
		}
		
		userDetails.setText(ul.showUserInfoByIndex(index));

		//show it somewhere else too?
	}

	
	/** 
	 * @param event
	 */
	public void clearText(ActionEvent event){
        newUsername.clear();
        newPassword.clear();
    }
}
